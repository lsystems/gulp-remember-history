'use strict';

var remember = require('../');
var should = require('should');
var sinon = require('sinon');
var util = require('gulp-util');
var File = util.File;

function makeTestFile(path, contents, history) {
  contents = contents || 'test file';
  var file = new File({
    path: path,
    contents: new Buffer(contents)
  });
  file.history = (history || []).concat(file.history);
  return file;
}

require('mocha');

describe('gulp-remember', function() {
  describe('remember', function() {
    it('should pass one file through a previously empty cache', function(done) {
      var stream = remember('passOneThrough');
      var file = makeTestFile('./fixture/file.js', 'just a file');
      var filesSeen = 0;
      stream.on('data', function(file) {
        file.path.should.equal('./fixture/file.js');
        file.contents.toString().should.equal('just a file');
        filesSeen++;
      });
      stream.once('end', function() {
        filesSeen.should.equal(1);
        done();
      });
      stream.write(file);
      stream.end();
    });

    it('should pass many files through a previously empty cache', function(done) {
      var stream = remember('manyFilesThrough');
      var filesSeen = 0;

      stream.on('data', function(file) {
        file.path.should.startWith('./fixture/');
        file.path.should.endWith('.js');
        file.contents.toString().should.startWith('file');
        filesSeen++;
      });
      stream.once('end', function() {
        filesSeen.should.equal(100);
        done();
      });
      for (var i = 0; i < 100; i++) {
        stream.write(makeTestFile('./fixture/' + i + '.js', 'file ' + i));
      }
      stream.end();
    });

    it('should remember the files passed through it on subsequent uses', function(done) {
      var stream = remember('remember');
      var anotherStream;
      var filesSeen = 0;
      var startingFiles = [makeTestFile('./fixture/one'), makeTestFile('./fixture/two')];
      var oneMoreFile = makeTestFile('./fixture/three');

      stream.resume(); // don't care about reading the files on this first go-round
      stream.once('end', function() {
        // done writing for the first time. write another file, then check we have three in total.
        anotherStream = remember('remember');
        anotherStream.on('data', function() {
          filesSeen++;
        });
        anotherStream.once('end', function() {
          filesSeen.should.equal(3);
          done();
        });
        anotherStream.write(oneMoreFile);
        anotherStream.end();
      });
      startingFiles.forEach(function(file) {
        stream.write(file);
      });
      stream.end();
    });

    it('should use the default cache when no name is passed', function(done) {
      var stream = remember();
      var anotherStream;
      var filesSeen = 0;

      stream.resume(); // don't care about reading the files on this first go-round
      stream.once('end', function() {
        anotherStream = remember();
        anotherStream.on('data', function() {
          filesSeen++;
        });
        anotherStream.on('end', function() {
          filesSeen.should.equal(2000);
          done();
        });
        for (var i = 1000; i < 2000; i++) {
          anotherStream.write(makeTestFile('./fixture/' + i + '.js'));
        }
        anotherStream.end();
      });
      for (var i = 0; i < 1000; i++) {
        stream.write(makeTestFile('./fixture/' + i + '.js'));
      }
      stream.end();
    });

    it('should not pass duplicates through', function(done) {
      var stream = remember('noDuplicates');
      var anotherStream;
      var filesSeen = 0;

      stream.resume();
      stream.once('end', function() {
        anotherStream = remember('noDuplicates');
        anotherStream.on('data', function() {
          filesSeen++;
        });
        anotherStream.on('end', function() {
          filesSeen.should.equal(6);
          done();
        });
        anotherStream.write(makeTestFile('./fixture/three'));
        anotherStream.write(makeTestFile('./fixture/four'));
        anotherStream.write(makeTestFile('./fixture/five'));
        anotherStream.write(makeTestFile('./fixture/six'));
        anotherStream.end();
      });
      stream.write(makeTestFile('./fixture/one'));
      stream.write(makeTestFile('./fixture/two'));
      stream.write(makeTestFile('./fixture/three'));
      stream.write(makeTestFile('./fixture/four'));
      stream.end();
    });
  });

  describe('forget', function() {
    it('should forget a file it used to know', function(done) {
      var stream = remember('forget');
      var anotherStream;
      var filesSeen = 0;

      stream.resume();
      stream.once('end', function() {
        remember.forget('forget', './fixture/one');
        anotherStream = remember('forget');
        anotherStream.on('data', function(file) {
          file.path.should.equal('./fixture/two');
          filesSeen++;
        });
        anotherStream.on('end', function() {
          filesSeen.should.equal(1);
          done();
        });
        anotherStream.write(makeTestFile('./fixture/two'));
        anotherStream.end();
      });
      stream.write(makeTestFile('./fixture/one'));
      stream.end();
    });

    it('should not throw when target cache does not exist', function() {
      (function() {
        remember.forget('peaceAndLove', 'some/file');
      }).should.not.throw();
    });

    it('should not throw when target cache exists but file does not', function() {
      remember('kittens');
      (function() {
        remember.forget('kittens', 'Mister_McButtercups');
      }).should.not.throw();
    });

    it('should log a warning when target cache does not exist', function() {
      var logStub = sinon.stub(util, 'log');

      remember.forget('peaceAndLove', 'some/file');
      logStub.called.should.be.true;

      var logArgs = logStub.args[0];

      // Should append the name of the plugin
      logArgs[0].should.equal('gulp-remember-history');

      // Should warn about the specific cache name
      logArgs[1].should.containEql('peaceAndLove');

      logStub.restore();
    });

    it('should log a warning when target files does not exist in target cache', function() {
      var logStub = sinon.stub(util, 'log');

      remember('cacheThatExists');
      remember.forget('cacheThatExists', 'file/that/doesnt/exist');
      logStub.called.should.be.true;

      var logArgs = logStub.args[0];

      // Should append the name of the plugin
      logArgs[0].should.equal('gulp-remember-history');

      // Should warn about the specific cache name
      logArgs[1].should.containEql('file/that/doesnt/exist');

      logStub.restore();
    });

    it('should forget a file it used to know', function(done) {
      var stream = remember('forgetUsingHistory');
      var anotherStream;
      var filesSeen = 0;

      stream.resume();
      stream.once('end', function() {
        remember.forget('forgetUsingHistory', './fixture/one');
        anotherStream = remember('forgetUsingHistory');
        anotherStream.on('data', function(file) {
          file.path.should.equal('./fixture/two');
          filesSeen++;
        });
        anotherStream.on('end', function() {
          filesSeen.should.equal(1);
          done();
        });
        anotherStream.write(makeTestFile('./fixture/two', '', ['./fixture/two.old']));
        anotherStream.end();
      });
      stream.write(makeTestFile('./fixture/one', '', ['./fixture/one.old']));
      stream.end();
    });

    it('should forget a file it used to know in its history', function(done) {
      var stream = remember('forgetUsingHistory');
      var anotherStream;
      var filesSeen = 0;

      stream.resume();
      stream.once('end', function() {
        remember.forget('forgetUsingHistory', './fixture/one.old');
        anotherStream = remember('forgetUsingHistory');
        anotherStream.on('data', function(file) {
          file.path.should.equal('./fixture/two');
          filesSeen++;
        });
        anotherStream.on('end', function() {
          filesSeen.should.equal(1);
          done();
        });
        anotherStream.write(makeTestFile('./fixture/two', '', ['./fixture/two.old']));
        anotherStream.end();
      });
      stream.write(makeTestFile('./fixture/one', '', ['./fixture/one.old']));
      stream.end();
    });
  });

  describe('forgetAll', function() {
    it('should forget all files in a populated cache', function(done) {
      var stream = remember('forgetAll');
      var anotherStream;
      var filesSeen = 0;

      stream.resume();
      stream.once('end', function() {
        remember.forgetAll('forgetAll');
        anotherStream = remember('forgetAll');
        anotherStream.on('data', function(file) {
          file.path.should.equal('./fixture/three');
          filesSeen++;
        });
        anotherStream.on('end', function() {
          filesSeen.should.equal(1);
          done();
        });
        anotherStream.write(makeTestFile('./fixture/three'));
        anotherStream.end();
      });
      stream.write(makeTestFile('./fixture/one'));
      stream.write(makeTestFile('./fixture/two'));
      stream.end();
    });

    it('should forget all files in the default cache', function(done) {
      var stream = remember();
      var anotherStream;
      var filesSeen = 0;

      stream.resume();
      stream.once('end', function() {
        remember.forgetAll();
        anotherStream = remember();
        anotherStream.on('data', function(file) {
          file.path.should.equal('./fixture/three');
          filesSeen++;
        });
        anotherStream.on('end', function() {
          filesSeen.should.equal(1);
          done();
        });
        anotherStream.write(makeTestFile('./fixture/three'));
        anotherStream.end();
      });
      stream.write(makeTestFile('./fixture/one'));
      stream.write(makeTestFile('./fixture/two'));
      stream.end();
    });

    it('should not throw when target cache does not exist', function() {
      (function() {
        remember.forgetAll('peanutButterJellyTime');
      }).should.not.throw();
    });

    it('should log a warning when target cache does not exist', function() {
      var logStub = sinon.stub(util, 'log');

      remember.forgetAll('peanutButterJellyTime');
      logStub.called.should.be.true;

      var logArgs = logStub.args[0];

      // Should append the name of the plugin
      logArgs[0].should.equal('gulp-remember-history');

      // Should warn about the specific cache name
      logArgs[1].should.containEql('peanutButterJellyTime');

      logStub.restore();
    });

    it('should not throw on subsequent forgetAll calls', function(done) {
      var stream = remember('forgetAllMulti');
      stream.resume();
      stream.once('end', function() {
        remember.forgetAll('forgetAllMulti');
        (function() {
          remember.forgetAll('forgetAllMulti');
        }).should.not.throw();
        done();
      });
      stream.write(makeTestFile('./what/ever'));
      stream.end();
    });
  });

  describe('cacheFor', function() {
    it('should return the named cache', function(done) {
      var stream = remember('cacheFor');

      stream.resume();
      stream.once('end', function() {
        var cache = remember.cacheFor('cacheFor');
        cache.should.be.an.instanceOf(Object);

        var file = cache['whitehouse/nuclear-codes.rtf'];
        file.should.be.an.instanceOf(Object);
        file.should.have.property('path', 'whitehouse/nuclear-codes.rtf');

        done();
      });
      stream.write(makeTestFile('whitehouse/nuclear-codes.rtf'));
      stream.end();
    });

    it('should return the default cache', function(done) {
      var stream = remember();

      stream.resume();
      stream.once('end', function() {
        var cache = remember.cacheFor();
        cache.should.be.an.instanceOf(Object);

        var file = cache['jennifer-lawrence/fully-clothed.jpg'];
        file.should.be.an.instanceOf(Object);
        file.should.have.property('path', 'jennifer-lawrence/fully-clothed.jpg');

        done();
      });
      stream.write(makeTestFile('jennifer-lawrence/fully-clothed.jpg'));
      stream.end();
    });

    it('should return nothing if given bogus cache name', function() {
      var cache = remember.cacheFor('speculation-on-the-guilt-or-innocence-of-adnan-syed');
      should.not.exist(cache);
    });
  });

  describe('historyFor', function() {
    it('should return the named history', function(done) {
      var stream = remember('historyFor');

      stream.resume();
      stream.once('end', function() {
        var history = remember.historyFor('historyFor');
        history.should.be.an.instanceOf(Object);

        var file = history['whitehouse/nuclear-codes.rtf'];
        file.should.be.equal('whitehouse/nuclear-codes.rtf');
        file = history['whitehouse/nuclear-codes.old.rtf'];
        file.should.be.equal('whitehouse/nuclear-codes.rtf');

        done();
      });
      stream.write(makeTestFile('whitehouse/nuclear-codes.rtf', '', ['whitehouse/nuclear-codes.old.rtf']));
      stream.end();
    });

    it('should return the default history', function(done) {
      var stream = remember();
      stream.resume();
      stream.once('end', function() {
        var history = remember.historyFor();
        history.should.be.an.instanceOf(Object);

        var file = history['jennifer-lawrence/fully-clothed.jpg'];
        file.should.be.equal('jennifer-lawrence/fully-clothed.jpg');
        file = history['jennifer-lawrence/fully-clothed.old.jpg'];
        file.should.be.equal('jennifer-lawrence/fully-clothed.jpg');

        done();
      });
      stream.write(makeTestFile('jennifer-lawrence/fully-clothed.jpg', '',
        ['jennifer-lawrence/fully-clothed.old.jpg']));
      stream.end();
    });

    it('should return nothing if given bogus history name', function() {
      var history = remember.historyFor('speculation-on-the-guilt-or-innocence-of-adnan-syed');
      should.not.exist(history);
    });
  });
});
