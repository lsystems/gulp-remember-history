'use strict';

var through = require('through2');
var util = require('gulp-util');
var pluginName = 'gulp-remember-history'; // name of our plugin for error logging purposes
var caches = {}; // will hold named file caches
var defaultName = '_default'; // name to give a cache if not provided

/**
 * Return a through stream that will:
 *   1. Remember all files that ever pass through it.
 *   2. Add all remembered files back into the stream when not present.
 * @param cacheName {string} Name to give your cache.
 *                           Caches with different names can know about different sets of files.
 */
function gulpRememberHistory(cacheName) {
  var cache; // the files we've ever put our hands on in the current stream

  if (cacheName !== undefined && typeof cacheName !== 'number' && typeof cacheName !== 'string') {
    throw new util.PluginError(pluginName, 'Usage: require("gulp-remember-history")(name); where name is undefined, ' +
      'number or string');
  }

  cacheName = cacheName || defaultName; // maybe need to use a default cache
  caches[cacheName] = caches[cacheName] || {files: {}, history: {}}; // maybe initialize the named cache
  cache = caches[cacheName];

  function transform(file, enc, cb) {
    cache.files[file.path] = file; // add file to our cache

    (file.history || []).forEach(function(path) {
      cache.history[path] = file.path;
    });

    cb();
  }

  function flush(cb) {
    // add all files we've ever seen back into the stream
    for (var path in cache.files) {
      if (cache.files.hasOwnProperty(path))
        this.push(cache.files[path]); // add this file back into the current stream
    }
    cb();
  }

  return through.obj(transform, flush);
}

function forget(cacheName, path) {
  (caches[cacheName].files[path].history || []).forEach(function(path) {
    delete caches[cacheName].history[path];
  });
  delete caches[cacheName].files[path];
}

/**
 * Forget about a file using its history.
 * A warning is logged if either the named cache or file do not exist.
 *
 * @param cacheName {string} name of the cache from which to drop the file
 * @param path {string} path of the file to forget
 */
gulpRememberHistory.forget = function(cacheName, path) {
  if (!path) {
    path = cacheName;
    cacheName = defaultName;
  }

  if (typeof cacheName !== 'number' && typeof cacheName !== 'string') {
    throw new util.PluginError(pluginName, 'Usage: require("gulp-remember-history").forget(cacheName, path); where ' +
      'cacheName is undefined, number or string and path is a string');
  }

  if (caches[cacheName] === undefined)
    return util.log(pluginName, '- .forget() warning: cache ' + cacheName + ' not found');

  if (caches[cacheName].files[path] !== undefined)
    return forget(cacheName, path);

  if (caches[cacheName].history[path] !== undefined)
    return forget(cacheName, caches[cacheName].history[path]);

  return util.log(pluginName, '- .forget() warning: file ' + path + ' not found in cache ' + cacheName);
};

/**
 * Forget all files in one cache.
 * A warning is logged if the cache does not exist.
 *
 * @param cacheName {string} name of the cache to wipe
 */
gulpRememberHistory.forgetAll = function(cacheName) {
  cacheName = cacheName || defaultName;

  if (typeof cacheName !== 'number' && typeof cacheName !== 'string') {
    throw new util.PluginError(pluginName, 'Usage: require("gulp-remember-history").forgetAll(cacheName); where ' +
      'cacheName is undefined, number or string');
  }

  if (caches[cacheName] === undefined)
    return util.log(pluginName, '- .forget() warning: cache ' + cacheName + ' not found');

  caches[cacheName] = {files: {}, history: {}};
};

/**
 * Return a raw cache by name.
 * Useful for checking state. Manually adding or removing files is NOT recommended.
 *
 * @param cacheName {string} name of the cache to retrieve
 */
gulpRememberHistory.cacheFor = function(cacheName) {
  cacheName = cacheName || defaultName;

  if (typeof cacheName !== 'number' && typeof cacheName !== 'string') {
    throw new util.PluginError(pluginName, 'Usage: require("gulp-remember-history").cacheFor(cacheName); where ' +
      'cacheName is undefined, number or string');
  }

  return (caches[cacheName] || {}).files;
};

/**
 * Return a raw history by name.
 * Useful for checking state. Manually adding or removing files is NOT recommended.
 *
 * @param cacheName {string} name of the cache to retrieve
 */
gulpRememberHistory.historyFor = function(cacheName) {
  cacheName = cacheName || defaultName;

  if (typeof cacheName !== 'number' && typeof cacheName !== 'string') {
    throw new util.PluginError(pluginName, 'Usage: require("gulp-remember-history").historyFor(cacheName); where ' +
      'cacheName is undefined, number or string');
  }

  return (caches[cacheName] || {}).history;
};

module.exports = gulpRememberHistory;
